//
//  AnnounceUITests.swift
//  AnnounceUITests
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import XCTest

class AnnounceUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    
    func testValidList() {
        let app = XCUIApplication()
        app.launch()
        expectation(for: NSPredicate(format: "count == 4"), evaluatedWith: app.tables.cells, handler: nil)
        waitForExpectations(timeout: 10, handler: nil)
        XCTAssertEqual(app.tables.cells.count, 4)
        XCTAssertNotNil(app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Villers-sur-Mer")/*[[".cells.containing(.staticText, identifier:\"1 500 000 €\")",".cells.containing(.staticText, identifier:\"GSL EXPLORE\")",".cells.containing(.staticText, identifier:\"Villers-sur-Mer\")"],[[[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/)
        app.tables/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"GSL EXPLORE")/*[[".cells.containing(.staticText, identifier:\"1 500 000 €\")",".cells.containing(.staticText, identifier:\"GSL EXPLORE\")"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .other).element.tap()
        let map = app.scrollViews.otherElements.maps.element
        XCTAssertTrue(map.exists)
    }
    
}
