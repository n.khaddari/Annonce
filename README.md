# Annonce
iOS app to display advertisement.

## Technical requirements
- Currently the app supports iOS 14.
- You'll need Xcode 12 to build the app.
- The app supports the dark mode

## Dependencies 
- RXSwift for reactive programming 
- KingFisher for image downnload