//
//  MockAdvertisementsService.swift
//  AnnounceTests
//
//  Created by Nabil El khaddari on 01/03/2021.
//

import Foundation
import RxSwift
@testable import Announce

final class MockAdvertisementsService: AdvertisementsService {
    var advertisements: AdvertisementList
    var error: APIError
    var isSuccess: Bool
    
    init(advertisements: AdvertisementList = AdvertisementList.mock(),
         error: APIError = .decode,
         success: Bool = true) {
        self.advertisements = advertisements
        self.error = error
        self.isSuccess = success
    }
    
    func request(from endpoint: AdvertisementApi) -> Observable<AdvertisementList> {
        return Observable.create { (observable) -> Disposable in
            if self.isSuccess {
                observable.onNext(self.advertisements)
            } else {
                observable.onError(self.error)
            }
            return Disposables.create()
        }
    }
}
