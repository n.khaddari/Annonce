//
//  MOckAdvertisements.swift
//  AnnounceTests
//
//  Created by Nabil El khaddari on 01/03/2021.
//

import Foundation
@testable import Announce

extension AdvertisementList {
    static func example() -> AdvertisementList? {
        let json = "{\"items\": [{\"bedrooms\": 4, \"city\": \"Villers-sur-Mer\",\"id\": 1,\"area\": 250.0,\"url\": \"https://v.seloger.com/s/crop/590x330/visuels/1/7/t/3/17t3fitclms3bzwv8qshbyzh9dw32e9l0p0udr80k.jpg\",\"price\": 1500000.0,\"professional\": \"GSL EXPLORE\",\"propertyType\": \"Maison - Villa\",\"rooms\": 8}],\"totalCount\": 1}"
        
        guard let data = json.data(using: .utf8, allowLossyConversion: false) else { return nil }
        do {
            let object = try JSONDecoder().decode(AdvertisementList.self, from: data)
            return object
        } catch {}
        return nil
    }
    static func mock() -> AdvertisementList {
        AdvertisementList(advertisements: [Advertisement(bedrooms: 1,
                                                         city: "paris",
                                                         id: 5, area: 100,
                                                         url: nil, price: 100,
                                                         professional: nil,
                                                         propertyType: nil, rooms: 7)], totalCount: 0)
    }
}
