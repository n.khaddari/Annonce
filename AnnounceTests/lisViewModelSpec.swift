//
//  lisViewModelSpec.swift
//  AnnounceTests
//
//  Created by Nabil El khaddari on 01/03/2021.
//

import XCTest
@testable import Announce
import RxSwift

class lisViewModelSpec: XCTestCase {
    var viewModel: AdvertisementListViewModelImpl!
    var mockAdvertisementsService: MockAdvertisementsService!
    var list: AdvertisementList!
    let disposeBag = DisposeBag()
    
    override func setUp()  {
        list = AdvertisementList.example() ?? AdvertisementList.mock()
        
    }
    
    func testViewModelServiceSuccess() {
        let listViewModel =  (list.advertisements ?? []).map {
            AdvertisementCellViewModel(advertisement: $0)
        }
        mockAdvertisementsService = MockAdvertisementsService(advertisements: list)
        viewModel = AdvertisementListViewModelImpl(service: mockAdvertisementsService)
        viewModel.getAdvertisement()
        viewModel.state.skip(1).subscribe(onNext: { state in
            switch state {
            case let .success(content):
                XCTAssertEqual(content, listViewModel)
            default:
                XCTAssertNotNil(nil)
            }
        } ).disposed(by: disposeBag)
    }
    
    func testViewModelServiceFailed() {
        mockAdvertisementsService = MockAdvertisementsService(advertisements: list, success: false)
        viewModel = AdvertisementListViewModelImpl(service: mockAdvertisementsService)
        viewModel.getAdvertisement()
        viewModel.state.skip(1).subscribe(onNext: { state in
            switch state {
            case let .failed(error):
                XCTAssertEqual(error.localizedDescription, APIError.decode.localizedDescription)
            default:
                XCTAssertNotNil(nil)
            }
        } ).disposed(by: disposeBag)
    }
    
    func testAdvertisementParsing() {
        XCTAssertNotNil(AdvertisementList.example())
    }
    
}

