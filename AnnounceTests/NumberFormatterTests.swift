//
//  NumberFormatterTests.swift
//  AnnounceTests
//
//  Created by Nabil El khaddari on 01/03/2021.
//

import XCTest
@testable import Announce

class NumberFormatterTests: XCTestCase {
    
    func testformattedPrice() {
        print(Array(NumberFormatter().formatted(price: 1500).unicodeScalars))
        XCTAssertEqual(NumberFormatter().formatted(price: 10).replacingOccurrences(of: "\u{A0}", with: " "), "10 €")
        XCTAssertEqual(NumberFormatter().formatted(price: 1500)
                        .replacingOccurrences(of: "\u{A0}", with: " ")
                        .replacingOccurrences(of: "\u{202F}", with: " "), "1 500 €")
    }


}
