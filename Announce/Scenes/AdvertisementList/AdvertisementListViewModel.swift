//
//  ListViewModel.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import Foundation
import RxSwift
import RxRelay

protocol AdvertisementListViewModel {
    var title: String { get }
    func getAdvertisement()
    var advertisements: [AdvertisementCellViewModel] { get }
    var state: BehaviorRelay<State> { get }
    func didSelectItem(at index: Int)
}

typealias State = ResultState<[AdvertisementCellViewModel]>
class AdvertisementListViewModelImpl: AdvertisementListViewModel {
    
    private let service: AdvertisementsService
    weak var coordinator: AdvertisementListCoordinator?
    
    private(set) var advertisements = [AdvertisementCellViewModel]()
    private(set) var state: BehaviorRelay<State> = BehaviorRelay(value: .loading)
    private(set) var title = "list_title".localized
    
    let disposeBag = DisposeBag()
    
    init(service: AdvertisementsService) {
        self.service = service
    }
    
    func getAdvertisement() {
        state.accept(.loading)
        service.request(from: .getAdvertisements).subscribe(onNext: { [weak self] list in
            guard let self = self else {
                return
            }
            self.advertisements = (list.advertisements ?? []).map {
                AdvertisementCellViewModel(advertisement: $0)
            }
            DispatchQueue.main.async {
                self.state.accept(.success(content: self.advertisements))
            }
        }, onError: { [weak self] error in
            guard let error = error as? APIError else {
                self?.state.accept(.failed(error: APIError.unknown))
                return
            }
            DispatchQueue.main.async {
                self?.state.accept(.failed(error: error))
            }

        }).disposed(by: disposeBag)
    }
    
    func didSelectItem(at index: Int) {
        let viewModel = self.advertisements[index]
        if let id = viewModel.id {
            coordinator?.onSelect(item: id)
        }
    }
}
