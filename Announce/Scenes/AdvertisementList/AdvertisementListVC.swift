//
//  AdvertisementListVC.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import UIKit
import RxSwift

class AdvertisementListVC: UIViewController {
    var viewModel: AdvertisementListViewModel!
    @IBOutlet weak var tableView: UITableView!
    let disposeBag = DisposeBag()
    
    lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .large)
        loader.center = view.center
        loader.hidesWhenStopped = true
        loader.startAnimating()
        return loader
    }()
    
    lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.reloadButton.addAction(UIAction(handler: { (_) in
            self.viewModel.getAdvertisement()
            self.errorView.removeFromSuperview()
        }), for: .touchUpInside)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribe()
        viewModel.getAdvertisement()
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
        
    }
    
    func setupView() {
        self.title = viewModel.title
        tableView.register(AdvertisementCell.self, forCellReuseIdentifier: "AdvertisementCell")
        tableView.tableFooterView = UIView() 
    }
    
    func subscribe() {
        viewModel.state.subscribe(onNext: { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .loading:
                self.view.addSubview(self.loader)
            case .success:
                self.loader.stopAnimating()
                self.tableView.reloadData()
            case let .failed( error):
                self.loader.stopAnimating()
                self.view.addSubview(self.errorView)
                self.errorView.heightAnchor.constraint(equalToConstant: 140).isActive = true
                self.errorView.widthAnchor.constraint(equalToConstant: 200).isActive = true
                self.errorView.centerToSuperView()
                self.errorView.setup(error: error)
            }
        } ).disposed(by: disposeBag)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
        
    }
}

extension AdvertisementListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.advertisements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementCell", for: indexPath) as? AdvertisementCell else {
            return UITableViewCell()
        }
        let cellViewModel = viewModel.advertisements[indexPath.row]
        cell.updateWith(cellViewModel)
        return cell
    }
}

extension AdvertisementListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectItem(at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
