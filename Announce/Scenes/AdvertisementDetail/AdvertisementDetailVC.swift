//
//  AdvertisementDetailVC.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import UIKit
import Kingfisher
import RxSwift

import MapKit

class AdvertisementDetailVC: UIViewController {
    
    var viewModel: AdvertisementDetailViewModel!
    let disposeBag = DisposeBag()
    var advertisement: AdvertisementDetailModel?
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var propertyImageView: UIImageView!
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var roomsLabel: UILabel!
    @IBOutlet weak var bedRoomsLabel: UILabel!
    @IBOutlet weak var roomsView: UIView!
    @IBOutlet weak var bedRoomsView: UIView!
    @IBOutlet weak var mapContainer: UIView!
    
    lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .large)
        loader.center = view.center
        loader.hidesWhenStopped = true
        loader.startAnimating()
        return loader
    }()
    
    lazy var errorView: ErrorView = {
        let view = ErrorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.reloadButton.addAction(UIAction(handler: { (_) in
            self.viewModel.getAdvertisement()
            self.errorView.removeFromSuperview()
        }), for: .touchUpInside)
        return view
    }()
    
    lazy var mapView: MapRegionView =  {
        let map = MapRegionView()
        map.translatesAutoresizingMaskIntoConstraints = false
        if let city = self.advertisement?.city,
           let area = self.advertisement?.area {
            map.setup(city: city, area: area)
        }
        return map
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribe()
        viewModel.getAdvertisement()
    }
    
    func setupView() {
        guard let advertisement = advertisement else { return }
        cityLabel.text = advertisement.city
        ownerLabel.text = advertisement.professional
        priceLabel.text = advertisement.price
        if let rooms = advertisement.rooms {
            roomsLabel.text = "\(rooms)"
        } else {
            roomsView.isHidden = true
        }
        if let bedRooms = advertisement.bedrooms {
            bedRoomsLabel.text = "\(bedRooms)"
        } else {
            bedRoomsView.isHidden = true
        }
        
        propertyImageView.kf.setImage(with: advertisement.url) { [weak self]result in
            switch result {
            case .failure:
                self?.propertyImageView.image = UIImage(named: "houseDefault")
                break
            case .success:
                break
            }
        }
        mapContainer.addSubview(mapView)
        mapView.pinToSuperView()
        
    }
    
    func subscribe() {
        viewModel.state.subscribe(onNext: { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .loading:
                self.view.addSubview(self.loader)
            case let .success(item):
                self.loader.stopAnimating()
                self.advertisement = item
                self.setupView()
            case let .failed(error):
                self.loader.stopAnimating()
                self.view.addSubview(self.errorView)
                self.errorView.heightAnchor.constraint(equalToConstant: 140).isActive = true
                self.errorView.widthAnchor.constraint(equalToConstant: 200).isActive = true
                self.errorView.centerToSuperView()
                self.errorView.setup(error: error)
            }
        } ).disposed(by: disposeBag)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }
}
