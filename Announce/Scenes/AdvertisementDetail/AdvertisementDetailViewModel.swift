//
//  AdvertisementDetailViewModel.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import Foundation
import RxSwift
import RxRelay

protocol AdvertisementDetailViewModel {
    var state: BehaviorRelay<DetailState> { get }
    func getAdvertisement()
    func viewDidDisappear()
}

typealias DetailState = ResultState<AdvertisementDetailModel>

class AdvertisementDetailViewModelImpl: AdvertisementDetailViewModel {
    
    private let service: AdvertisementsServiceDetail
    let advertisementId: Int
    weak var coordinator: AdvertisementDetailCoordinator?
    
    private(set) var state: BehaviorRelay<DetailState> = BehaviorRelay(value: .loading)
    
    let disposeBag = DisposeBag()
    
    init(advertisementId: Int, service: AdvertisementsServiceDetail) {
        self.advertisementId = advertisementId
        self.service = service
    }
    
    func getAdvertisement() {
        service.request(from: .getAdvertisementDetail(id: advertisementId)).subscribe(onNext: { [weak self] response in
            guard let self = self else {
                return
            }
            let advertisement = AdvertisementDetailModel(advertisement: response)
            DispatchQueue.main.async {
                self.state.accept(.success(content: advertisement))
            }
        }, onError: { [weak self] error in
            guard let error = error as? APIError else {
                self?.state.accept(.failed(error: APIError.unknown))
                return
            }
            DispatchQueue.main.async {
                self?.state.accept(.failed(error: error))
            }

        }).disposed(by: disposeBag)
    }
    
    func viewDidDisappear() {
        coordinator?.didFinish()
    }
    
}
