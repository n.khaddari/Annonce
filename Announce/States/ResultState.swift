//
//  ResultState.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import Foundation

enum ResultState<T: Equatable> {
    case loading
    case success(content: T)
    case failed(error: Error)
}
