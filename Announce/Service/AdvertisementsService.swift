//
//  AdvertisementsService.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import Foundation
import RxSwift

protocol AdvertisementsService {
    func request(from endpoint: AdvertisementApi) -> Observable<AdvertisementList>
}

class AdvertisementsServiceImpl: AdvertisementsService {
    
    func request(from endpoint: AdvertisementApi) -> Observable<AdvertisementList> {
        return RequestWrapper.request(from: endpoint.urlRequest)
    }
    
}
