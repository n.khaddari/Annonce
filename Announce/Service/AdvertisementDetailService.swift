//
//  AdvertisementDetailService.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import Foundation
import RxSwift

protocol AdvertisementsServiceDetail {
    func request(from endpoint: AdvertisementApi) -> Observable<Advertisement>
}

class AdvertisementsServiceDetailImpl: AdvertisementsServiceDetail {
    
    func request(from endpoint: AdvertisementApi) -> Observable<Advertisement> {
        return RequestWrapper.request(from: endpoint.urlRequest)
    }
    
}
