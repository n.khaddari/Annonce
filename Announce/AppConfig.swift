//
//  AppConfig.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import Foundation

struct AppConfig {
    static let baseUrl = URL(string: "https://gsl-apps-technical-test.dignp.com")!
}
