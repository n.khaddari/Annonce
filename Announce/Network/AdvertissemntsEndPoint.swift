//
//  AdvertissemntsEndPoint.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import Foundation

protocol APIBuilder {
    var urlRequest: URLRequest { get }
    var baseURL: URL { get }
    var path: String { get }
}

enum AdvertisementApi {
    case getAdvertisements
    case getAdvertisementDetail(id: Int)
}

extension AdvertisementApi: APIBuilder {
    
    var baseURL: URL { AppConfig.baseUrl }
    
    var path: String {
        switch self {
        case .getAdvertisements:
            return "/listings.json"
        case let .getAdvertisementDetail(id):
            return "/listings/\(id).json"
        }
    }
    
    var urlRequest: URLRequest {
        URLRequest(url: self.baseURL.appendingPathComponent(path), cachePolicy: .reloadIgnoringLocalCacheData)
    }
}
