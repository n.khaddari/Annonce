//
//  AdvertisementDetailViewModel.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import UIKit

struct AdvertisementDetailModel: Equatable {
    let bedrooms: Int?
    let city: String?
    let id, area: Int?
    let url: URL?
    let price: String?
    let professional: String?
    let propertyType: PropertyType?
    let rooms: Int?
    
    init(advertisement: Advertisement) {
        bedrooms = advertisement.bedrooms
        city = advertisement.city
        id = advertisement.id
        if let intPrice = advertisement.price {
            price = NumberFormatter().formatted(price: intPrice)
        } else {
            price = nil
        }
        propertyType = advertisement.propertyType
        if let urlString = advertisement.url {
            url = URL(string: urlString)
        } else {
            url = nil
        }
        rooms = advertisement.rooms
        professional = advertisement.professional
        area = advertisement.area
    }
    
    init(advertisement: AdvertisementCellViewModel) {
        bedrooms = advertisement.bedrooms
        city = advertisement.city
        id = advertisement.id
        price = advertisement.price
        propertyType = advertisement.propertyType
        url = advertisement.url
        rooms = advertisement.rooms
        professional = advertisement.professional
        area = nil
    }
}
