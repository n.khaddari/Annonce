//
//  AdvertisementCellViewModel.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import Foundation

struct AdvertisementCellViewModel: Equatable {
    let bedrooms: Int?
    let city: String?
    let id: Int?
    let url: URL?
    let price: String?
    let propertyType: PropertyType?
    let rooms: Int?
    let professional: String?
    init(advertisement: Advertisement) {
        bedrooms = advertisement.bedrooms
        city = advertisement.city
        id = advertisement.id
        if let intPrice = advertisement.price {
            price = NumberFormatter().formatted(price: intPrice)
        } else {
            price = nil
        }
        propertyType = advertisement.propertyType
        if let urlString = advertisement.url {
            url = URL(string: urlString)
        } else {
            url = nil
        }
        rooms = advertisement.rooms
        professional = advertisement.professional
    }

}
