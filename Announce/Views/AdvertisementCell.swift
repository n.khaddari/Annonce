//
//  AdvertisementCell.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import UIKit
import Kingfisher

final class AdvertisementCell: UITableViewCell {
    private let cityLabel = UILabel()
    private let priceLabel = UILabel()
    private let propertyImageView = UIImageView()
    private let verticalStackView = UIStackView()
    private let horizontalStackView = UIStackView()
    private let propertyTypeImageView = UIImageView()
    private let professionalLabel = UILabel()
    private let backView = UIView()
    private let shadowLayer = ShadowView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        setupHierarchy()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        [cityLabel, priceLabel, propertyImageView, professionalLabel, backView, shadowLayer,
         verticalStackView, propertyTypeImageView, horizontalStackView]
            .forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
            }
        cityLabel.font = .systemFont(ofSize: 22, weight: .medium)
        priceLabel.font = .systemFont(ofSize: 22, weight: .semibold)
        verticalStackView.axis = .vertical
        horizontalStackView.axis = .horizontal
        propertyImageView.contentMode = .scaleAspectFit
        backView.backgroundColor = .systemGray5
        self.backView.layer.cornerRadius = 8
        self.backView.layer.masksToBounds = true
        contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        cityLabel.accessibilityLabel = "cityLabel"
    }
    
    private func setupHierarchy() {
        contentView.addSubview(shadowLayer)
        contentView.addSubview(backView)
        
        backView.addSubview(propertyImageView)
        backView.addSubview(propertyTypeImageView)
        backView.addSubview(horizontalStackView)
        
        horizontalStackView.addArrangedSubview(verticalStackView)
        horizontalStackView.addArrangedSubview(priceLabel)
        horizontalStackView.alignment = .center
        horizontalStackView.distribution = .equalSpacing
        
        verticalStackView.addArrangedSubview(cityLabel)
        verticalStackView.addArrangedSubview(professionalLabel)
    }
    
    private func setupLayout() {
        backView.pinToSuperView( constant: 16)
        shadowLayer.pinToSuperView( constant: 16)
        propertyImageView.pinToSuperView([.top], constant: 0)
        propertyImageView.pinToSuperView([.left, .right], constant: 16)
        propertyImageView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        horizontalStackView.pinToSuperView([.bottom, .left, .right], constant: 16)
        propertyTypeImageView.pinToSuperView([.top, .left], constant: 16)
        horizontalStackView.topAnchor.constraint(equalTo: propertyImageView.bottomAnchor, constant: 8).isActive = true
    }
    
    func updateWith(_ viewModel: AdvertisementCellViewModel) {
        self.priceLabel.text = viewModel.price
        self.cityLabel.text = viewModel.city
        self.professionalLabel.text = viewModel.professional
        self.propertyImageView.kf.setImage(with: viewModel.url) { [weak self] result in
            switch result {
            case .failure:
                self?.propertyImageView.image = UIImage(named: "houseDefault")
                break
            case .success:
                break
            }
        }
    }
    
    
}
