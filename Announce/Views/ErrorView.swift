//
//  ErrorView.swift
//  Announce
//
//  Created by Nabil El khaddari on 28/02/2021.
//

import UIKit

class ErrorView: UIStackView {
    private var errorImageView = UIImageView()
    private var labelError = UILabel()
    var reloadButton = UIButton()
    
    func setup(error: Error) {
        errorImageView.image = UIImage(systemName: "exclamationmark.icloud.fill")
        errorImageView.tintColor = .systemBlue
        errorImageView.contentMode = .scaleAspectFit
        
        reloadButton.setTitle("reload".localized, for: .normal)
        reloadButton.setTitleColor(.systemBlue, for: .normal)
        reloadButton.backgroundColor = .white
        reloadButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        reloadButton.layer.cornerRadius = 5
        reloadButton.layer.borderWidth = 1
        
        axis = .vertical
        alignment = .center
        distribution = .equalCentering
        
        labelError.text = error.localizedDescription
        
        setupHierarchy()
        setupLayout()
    }
    
    private func setupHierarchy() {
        addArrangedSubview(errorImageView)
        addArrangedSubview(labelError)
        addArrangedSubview(reloadButton)
    }
    
    private func setupLayout() {
        errorImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        errorImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }
}
