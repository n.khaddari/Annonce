//
//  MapRegionView.swift
//  Announce
//
//  Created by Nabil El khaddari on 28/02/2021.
//

import MapKit
import CoreLocation

class MapRegionView: MKMapView {

    func setup(city: String, area: Int) {
        self.delegate = self
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(city) { (placeMarks, error) in
            guard let placeMarks = placeMarks,
                let location = placeMarks.first?.location else {
                return
            }
            let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                      latitudinalMeters: Double(area) * 3, longitudinalMeters: Double(area) * 3)
            self.setRegion(coordinateRegion, animated: true)
            let circle = MKCircle(center: location.coordinate, radius: CLLocationDistance(area))
            self.addOverlay(circle)
        }
    }
}

extension MapRegionView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circle = MKCircleRenderer(overlay: overlay)
        circle.strokeColor = UIColor.blue
        circle.fillColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.1)
        circle.lineWidth = 1
        return circle
    }
}
