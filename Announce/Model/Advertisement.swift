//
//  Advertisement.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import Foundation

// MARK: - AdvertisementList
struct AdvertisementList: Codable {
    let advertisements: [Advertisement]?
    let totalCount: Int?

    enum CodingKeys: String, CodingKey {
        case advertisements = "items"
        case totalCount
    }
}

// MARK: - Advertisement
struct Advertisement: Codable {
    let bedrooms: Int?
    let city: String?
    let id, area: Int?
    let url: String?
    let price: Int?
    let professional: String?
    let propertyType: PropertyType?
    let rooms: Int?
}

// MARK: - PropertyType
enum PropertyType: String, Codable {
    case houseAndVilla = "Maison - Villa"
}
