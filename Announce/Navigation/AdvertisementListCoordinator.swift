//
//  AdvertisementListCoordinator.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import UIKit

final class AdvertisementListCoordinator: Coordinator {
    private(set) var childCoordinators: [Coordinator] = []
    private let navigationController: UINavigationController!
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let listViewController = AdvertisementListVC.newSelfFromNib()
        let listViewModel = AdvertisementListViewModelImpl(service: AdvertisementsServiceImpl())
        listViewModel.coordinator = self
        listViewController.viewModel = listViewModel
        self.navigationController.setViewControllers([listViewController], animated: false)
    }
    
    func childDidFinish(_ childCoordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: { coordinator-> Bool in
            return childCoordinator === coordinator
        }) {
            childCoordinators.remove(at: index)
        }
    }
    
    func onSelect(item: Int) {
        let advertisementDetailCoordinator = AdvertisementDetailCoordinator(navigationController: self.navigationController,
                                                                            idAdvertisement: item)
        advertisementDetailCoordinator.parentCoordinator = self
        childCoordinators.append(advertisementDetailCoordinator)
        advertisementDetailCoordinator.start()
    }
}
