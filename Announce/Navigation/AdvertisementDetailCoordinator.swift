//
//  AdvertisementDetailCoordinator.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import UIKit

final class AdvertisementDetailCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    private let navigationController: UINavigationController!
    private let idAdvertisement: Int
    var parentCoordinator: AdvertisementListCoordinator?
    
    
    init(navigationController: UINavigationController, idAdvertisement: Int) {
        self.navigationController = navigationController
        self.navigationController.navigationItem.largeTitleDisplayMode = .automatic
        self.idAdvertisement = idAdvertisement
    }
    
    func start() {
        let advertisementDetailVC = AdvertisementDetailVC.newSelfFromNib()
        let service = AdvertisementsServiceDetailImpl()
        let viewModel = AdvertisementDetailViewModelImpl(advertisementId: idAdvertisement,
                                                         service: service)
        viewModel.coordinator = self
        advertisementDetailVC.viewModel = viewModel
        
        self.navigationController.pushViewController(advertisementDetailVC, animated: true)
        
    }
    
    func didFinish() {
        parentCoordinator?.childDidFinish(self)
    }
    
}
