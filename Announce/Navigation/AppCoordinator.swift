//
//  AppCoordinator.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get }
    func start()
}

final class AppCoordinator: Coordinator {
    private(set) var childCoordinators: [Coordinator] = []
    private let window: UIWindow
    init(window: UIWindow) {
        self.window = window
    }
    func start() {
        let navigationController = UINavigationController()
        
        let advertisementListCoordinator = AdvertisementListCoordinator(navigationController: navigationController)
        childCoordinators.append(advertisementListCoordinator)
        advertisementListCoordinator.start()
        
        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
    }
    
    
}
