//
//  NumberFormatter+Extensions.swift
//  Announce
//
//  Created by Nabil El khaddari on 28/02/2021.
//

import Foundation

extension NumberFormatter {
    func formatted(price: Int) -> String {
        locale = Locale(identifier: "fr_FR")
        currencySymbol = "€"
        numberStyle = .currency
        maximumFractionDigits = 0
        minimumIntegerDigits = 1
        return self.string(for: price) ?? ""
    }
}
