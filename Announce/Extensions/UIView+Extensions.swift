//
//  UIView+Extensions.swift
//  Announce
//
//  Created by Nabil El khaddari on 27/02/2021.
//

import UIKit

enum Edge {
    case top
    case bottom
    case right
    case left
}

extension UIView {
    func pinToSuperView(_ edges: [Edge] = [.left, .right, .top, .bottom], constant: CGFloat = 0) {
        guard let superView = superview else { return}
        edges.forEach { (edge) in
            switch edge {
            case .left:
                leftAnchor.constraint(equalTo: superView.leftAnchor, constant: constant).isActive = true
            case .top:
                topAnchor.constraint(equalTo: superView.topAnchor, constant: constant).isActive = true
            case .bottom:
                bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -constant).isActive = true
            case .right:
                rightAnchor.constraint(equalTo: superView.rightAnchor, constant: -constant).isActive = true
            }
            
        }
    }
    func centerToSuperView() {
        guard let superView = superview else { return}
        centerXAnchor.constraint(equalTo: superView.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: superView.centerYAnchor).isActive = true
    }
}

class ShadowView: UIView {
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }

    private func setupShadow() {
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
