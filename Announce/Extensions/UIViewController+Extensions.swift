//
//  Utilis+UIViewController.swift
//  Announce
//
//  Created by Nabil El khaddari on 26/02/2021.
//

import UIKit

extension UIViewController {
    
    static func newSelfFromNib() -> Self {
        let nibName = String(describing: self)
        return self.init(nibName: nibName, bundle: nil)
    }
}
